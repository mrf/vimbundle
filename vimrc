" -1- Must-come-first stuff ---------------------------------------------- {{{
let VIM_HOME=expand('$HOME', 1) . '/.vim'
let g:my_vimrc=$MYVIMRC

set nocompatible " Don't care about vi compatibility
syntax enable " Enable syntax highlighting

filetype off

" Use , as the leader
let mapleader = "\<F12>"
let g:mapleader = "\<F12>"
map <Space> <Leader>

" ------------------------------------------------------------------------ }}}

" -2a- dein plugins ------------------------------------------------------ {{{
if has("nvim")
set runtimepath+=~/.dein/repos/github.com/Shougo/dein.vim
call dein#begin('~/.dein')
call dein#add('Shougo/dein.vim')

call dein#add('Shougo/deoplete.nvim') " {{{
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1

inoremap <silent><expr> <TAB>
		\ pumvisible() ? "\<C-n>" :
		\ <SID>check_back_space() ? "\<TAB>" :
		\ deoplete#mappings#manual_complete()

function! s:check_back_space() abort "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction "}}}
" }}}

call dein#add('ctrlpvim/ctrlp.vim') " {{{
" CtrlP bindings
let g:ctrlp_map = '<leader>i'
" map <leader>i :CtrlP<CR>
map <leader>o :CtrlPBuffer<CR>
map <leader>u :CtrlPMRU<CR>

let g:ctrlp_custom_ignore = '\v[\/](\.git|\.hg|\.svn|node_modules)$'
let g:ctrlp_match_window = 'bottom'
let g:ctrlp_user_command = {
      \ 'types': {
        \ 1: ['.git/', 'cd %s && git ls-files'],
        \ 2: ['.hg/', 'hg --cwd %s locate -I .'],
        \ },
      \ 'fallback': 'find %s -type f'
      \ }

" }}}
"
call dein#add('scrooloose/syntastic') " {{{
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height = 6
let g:syntastic_python_flake8_args="--ignore=E501"
let g:syntastic_typescript_tsc_fname = ''

let g:syntastic_mode_map = {
    \ "mode": "passive",
    \ "active_filetypes": ["javascript", "coffee", "python", "typescript"] }
"}}}
"
call dein#add('scrooloose/nerdtree') " {{{
let g:NERDTreeDirArrows=1
let g:NERDTreeQuitOnOpen=1
let g:NERDTreeShowHidden=1
let g:NERDTreeDirArrowExpandable = '‣'
let g:NERDTreeDirArrowCollapsible = '⯆'

" Set the key to toggle NERDTree
nnoremap <leader>d :NERDTreeToggle<cr>
nnoremap <leader>e :NERDTreeFind<CR>

" set Enter/Return to activate a node
let NERDTreeMapActivateNode='<CR>'
let NERDTreeIgnore=['\.pyc$']

" }}}

call dein#add('scrooloose/nerdcommenter') " {{{
nmap \c <Plug>NERDCommenterToggle
vmap \c <Plug>NERDCommenterToggle

let g:NERDSpaceDelims=1
" }}}

call dein#add('godlygeek/csapprox')

call dein#add('vim-scripts/BufOnly.vim')
call dein#add('hynek/vim-python-pep8-indent')
call dein#add('rking/ag.vim')
call dein#add('Valloric/MatchTagAlways')
call dein#add('tmhedberg/matchit')
call dein#add('othree/javascript-libraries-syntax.vim')
call dein#add('airblade/vim-gitgutter')
call dein#add('othree/yajs.vim')
call dein#add('tpope/vim-surround')
call dein#add('tpope/vim-repeat')
call dein#add('Konfekt/FastFold')

call dein#add('davidhalter/jedi-vim') " {{{
let g:jedi#documentation_command = "<leader>k"
let g:jedi#completions_enabled = 0
let g:jedi#auto_vim_configuration = 0
if has('python3')
    let g:jedi#force_py_version = 3
endif
" }}}
"
call dein#add('zchee/deoplete-jedi') " {{{

"}}}

call dein#add('bling/vim-airline') " {{{
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#fnamemod = ':t'
" }}}

call dein#add('vim-airline/vim-airline-themes') " {{{
let g:airline_theme = 'tomorrow'
" }}}

call dein#add('vim-scripts/Spiffy-Foldtext') " {{{
let g:SpiffyFoldtext_format="%c{·}   ·····< %n >·%<%f{·}"
" }}}

call dein#add('mhinz/vim-startify') " {{{
" let g:startify_custom_header = startify#fortune#cowsay()
let g:startify_session_dir = '~/.vim/sessions'
" }}}

call dein#add('tpope/vim-unimpaired') " {{{
" }}}

call dein#add('svermeulen/vim-easyclip') " {{{
let g:EasyClipUseSubstituteDefaults = 1
let g:EasyClipShareYanks = 1
let g:EasyClipAutoFormat = 1
let g:EasyClipAlwaysMoveCursorToEndOfPaste = 1
let g:EasyClipUseCutDefaults = 0
let g:EasyClipPreserveCursorPositionAfterYank = 1
let g:EasyClipEnableBlackHoleRedirect = 0

nmap \\ <plug>EasyClipToggleFormattedPaste
imap <c-v> <plug>EasyClipInsertModePaste

nmap M <Plug>MoveMotionLinePlug
vmap M <Plug>MoveMotionPlug
nnoremap gm m
nmap Y :EasyClipBeforeYank<CR>yy:EasyClipOnYanksChanged<CR>

" }}}

call dein#add('othree/html5.vim')
call dein#add('unblevable/quick-scope')

call dein#add('roosta/srcery')

call dein#end()
endif
"}}}

" -2- Vundle plugins ----------------------------------------------------- {{{
if !has("nvim")
let &rtp.="," . VIM_HOME . '/bundle/Vundle.vim'

call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'tpope/vim-repeat'

Plugin 'scrooloose/syntastic' " {{{
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height = 6
let g:syntastic_python_flake8_args="--ignore=E501"
let g:syntastic_typescript_tsc_fname = ''

let g:syntastic_mode_map = {
    \ "mode": "passive",
    \ "active_filetypes": ["javascript", "coffee", "python", "typescript"] }
" }}}

Plugin 'ctrlpvim/ctrlp.vim' " {{{
" CtrlP bindings
let g:ctrlp_map = '<leader>i'
" map <leader>i :CtrlP<CR>
map <leader>o :CtrlPBuffer<CR>
map <leader>u :CtrlPMRU<CR>

let g:ctrlp_custom_ignore = '\v[\/](\.git|\.hg|\.svn|node_modules)$'
let g:ctrlp_match_window = 'bottom'
let g:ctrlp_user_command = {
      \ 'types': {
        \ 1: ['.git/', 'cd %s && git ls-files'],
        \ 2: ['.hg/', 'hg --cwd %s locate -I .'],
        \ },
      \ 'fallback': 'find %s -type f'
      \ }

" }}}

Plugin 'Valloric/YouCompleteMe' " {{{
" }}}

Plugin 'davidhalter/jedi-vim' " {{{
let g:jedi#documentation_command = "<leader>k"
let g:jedi#completions_enabled = 0
let g:jedi#auto_vim_configuration = 0
if has('python3')
    let g:jedi#force_py_version = 3
endif
" }}}

Plugin 'SirVer/ultisnips' " {{{
let g:UltiSnipsExpandTrigger="<C-j>"
let g:UltiSnipsJumpForwardTrigger="<C-j>"
let g:UltiSnipsJumpBackwardTrigger="<C-k>"
" }}}

Plugin 'scrooloose/nerdcommenter' " {{{
nmap \c <Plug>NERDCommenterToggle
vmap \c <Plug>NERDCommenterToggle

let g:NERDSpaceDelims=1
" }}}

Plugin 'scrooloose/nerdtree' " {{{
let g:NERDTreeDirArrows=1
let g:NERDTreeQuitOnOpen=1
let g:NERDTreeShowHidden=1

" Set the key to toggle NERDTree
nnoremap <leader>d :NERDTreeToggle<cr>
nnoremap <leader>e :NERDTreeFind<CR>

" set Enter/Return to activate a node
let NERDTreeMapActivateNode='<CR>'
let NERDTreeIgnore=['\.pyc$']

" }}}

Plugin 'bling/vim-airline' " {{{
if has("gui")
    let g:airline_powerline_fonts = 1
else
    let g:airline_powerline_fonts = 0
endif
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline_theme = "PaperColor"
function! SetupAirline()
    function! Modified()
        return &modified ? " *" : ''
    endfunc

    call airline#parts#define_raw('filename', '%<%f')
    call airline#parts#define_function('modified', 'Modified')

    let g:airline_mode_map = {
                \ '__' : '-',
                \ 'n'  : 'N',
                \ 'i'  : 'I',
                \ 'R'  : 'R',
                \ 'c'  : 'C',
                \ 'v'  : 'V',
                \ 'V'  : 'V',
                \ '' : 'V',
                \ 's'  : 'S',
                \ 'S'  : 'S',
                \ '' : 'S',
                \ }

    let g:airline_exclude_filenames = ['fugitive:']

    let g:airline_section_b = airline#section#create_left(['filename'])
    let g:airline_section_c = airline#section#create([''])
    let g:airline_section_gutter = airline#section#create(['modified', '%='])
    let g:airline_section_x = airline#section#create_right(['branch'])
    let g:airline_section_y = airline#section#create_right(['filetype'])
    let g:airline_section_z = airline#section#create(['%{getcwd()}'])
endfunc

augroup airline_custom
    autocmd!

    autocmd VimEnter * call SetupAirline()
    autocmd BufWinEnter fugitive:* let g:airline_section_a = airline#section#create(['FUGITIVE'])
    autocmd BufLeave fugitive:* let g:airline_section_a = airline#section#create(['mode'])
augroup END

" }}}

Plugin 'easymotion/vim-easymotion' " {{{
let g:EasyMotion_use_smartsign_us = 1
let g:EasyMotion_smartcase = 1
" }}}

Plugin 'mattn/emmet-vim' " {{{
imap <C-e> <C-y>,
"let g:user_emmet_install_global = 0
"autocmd FileType html,css EmmetInstall
" }}}

Plugin 'terryma/vim-expand-region' " {{{
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)
" }}}

Plugin 'airblade/vim-rooter' " {{{
if !exists('g:rooter_patterns')
  let g:rooter_patterns = ['.git', '.git/', '_darcs/', '.hg/', '.bzr/', '.svn/']
endif

let g:rooter_patterns = g:rooter_patterns + ['README.md', 'package.json']
" }}}

Plugin 'xolox/vim-session' " {{{
let g:session_directory = "~/.vim/sessions"
set sessionoptions-=tabpages
let g:session_autoload = "no"
let g:session_autosave = "yes"
let g:session_autosave_periodic = 3

nnoremap <F2> :OpenSession<CR>

" }}}

Plugin 'tpope/vim-fugitive' " {{{
nnoremap <leader>gd :Gvdiff<CR>
nnoremap <leader>gc :Gcommit -v<CR>
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gw :Gwrite<CR>
" }}}

Plugin 'rhysd/clever-f.vim' " {{{
let g:clever_f_across_no_line = 1
let g:clever_f_timeout_ms = 2000
let g:clever_f_chars_match_any_signs = ''
let g:clever_f_fix_key_direction = 1
" }}}

Plugin 'godlygeek/csapprox'
Plugin 'kchmck/vim-coffee-script'
Plugin 'vim-scripts/BufOnly.vim'
Plugin 'hynek/vim-python-pep8-indent'
Plugin 'moll/vim-node'
Plugin 'rking/ag.vim'
Plugin 'digitaltoad/vim-jade.git'
Plugin 'Valloric/MatchTagAlways'
Plugin 'tmhedberg/matchit'
Plugin 'honza/vim-snippets'
Plugin 'othree/javascript-libraries-syntax.vim'
Plugin 'tpope/vim-dispatch'
Plugin 'airblade/vim-gitgutter'
Plugin 'othree/yajs.vim'
Plugin 'tpope/vim-surround'
Plugin 'leafgarland/typescript-vim.git'

Plugin 'Quramy/tsuquyomi' " {{{
let g:tsuquyomi_disable_quickfix = 1
let g:syntastic_typescript_checkers = ['tsuquyomi'] " You shouldn't use 'tsc' checker.
" }}}

Plugin 'Shougo/vimproc.vim'

Plugin 'vim-scripts/Spiffy-Foldtext' " {{{
let g:SpiffyFoldtext_format="%c{·}   ·····< %n >·%<%f{·}"
" }}}

Plugin 'vim-scripts/FastFold'

Plugin 'mhinz/vim-startify' " {{{
" let g:startify_custom_header = startify#fortune#cowsay()
let g:startify_session_dir = '~/.vim/sessions'
" }}}

Plugin 'tpope/vim-unimpaired' " {{{
" }}}

Plugin 'svermeulen/vim-easyclip' " {{{
let g:EasyClipUseSubstituteDefaults = 1
let g:EasyClipShareYanks = 1
let g:EasyClipAutoFormat = 1
let g:EasyClipAlwaysMoveCursorToEndOfPaste = 1
let g:EasyClipUseCutDefaults = 0
let g:EasyClipPreserveCursorPositionAfterYank = 1
let g:EasyClipEnableBlackHoleRedirect = 0

nmap \\ <plug>EasyClipToggleFormattedPaste
imap <c-v> <plug>EasyClipInsertModePaste

nmap M <Plug>MoveMotionLinePlug
vmap M <Plug>MoveMotionPlug
nnoremap gm m
nmap Y :EasyClipBeforeYank<CR>yy:EasyClipOnYanksChanged<CR>

" }}}

Plugin 'othree/html5.vim'
Plugin 'xolox/vim-shell'
Plugin 'xolox/vim-misc'
Plugin 'godlygeek/tabular'
Plugin 'unblevable/quick-scope'

Plugin 'kana/vim-submode' " {{{
let g:did_submode = 0
let g:submode_timeout = 0
let g:submode_timeoutlen = 3000
let g:submode_keep_leaving_key = 1

augroup submodefix
    autocmd!
    autocmd VimEnter,GUIEnter * :call InitSubmodeBindings()
augroup END

function! InitSubmodeBindings()
    if !g:did_submode
        " echom 'Initializing submode bindings'
        call submode#enter_with('window', 'n', '', '\w')
        call submode#leave_with('window', 'n', '', '<Esc>')

        " Vertical resizing using +, -
        call submode#map('window', 'n', '', '-', '<C-w>5-')
        call submode#map('window', 'n', '', '+', '<C-w>5+')
        call submode#map('window', 'n', '', '_', '<C-w>5-')
        call submode#map('window', 'n', '', '=', '<C-w>5+')

        " Horizontal resizing using < >
        call submode#map('window', 'n', '', '>', '<C-w>5>')
        call submode#map('window', 'n', '', '.', '<C-w>5>')
        call submode#map('window', 'n', '', '<', '<C-w>5<')
        call submode#map('window', 'n', '', ',', '<C-w>5<')

        " Movement
        call submode#map('window', 'n', '', 'h', '<C-w>h')
        call submode#map('window', 'n', '', 'j', '<C-w>j')
        call submode#map('window', 'n', '', 'k', '<C-w>k')
        call submode#map('window', 'n', '', 'l', '<C-w>l')

        " Moving windows
        call submode#map('window', 'n', '', 'H', '<C-w>H')
        call submode#map('window', 'n', '', 'J', '<C-w>J')
        call submode#map('window', 'n', '', 'K', '<C-w>K')
        call submode#map('window', 'n', '', 'L', '<C-w>L')

        " Make everything equal
        call submode#map('window', 'n', '', '*', '<C-w>=')

        call submode#enter_with('buffer', 'n', '', '\b')
        call submode#leave_with('buffer', 'n', '', '<Esc>')

        call submode#map('buffer', 'n', '', 'n', ':bnext<CR>')
        call submode#map('buffer', 'n', '', 'N', ':bprev<CR>')
        call submode#map('buffer', 'n', '', 'p', ':bprev<CR>')
        call submode#map('buffer', 'n', '', 'c', ':bdelete<CR>')
        call submode#map('buffer', 'n', '', 'd', ':bdelete<CR>')
        let g:did_submode = 1
    endif
endfunction
" }}}

Plugin 'sjl/gundo.vim' " {{{
nnoremap <F5> :GundoToggle<CR>
let g:gundo_close_on_revert = 1
" }}}

" Color schemes
Plugin 'NLKNguyen/papercolor-theme'
Plugin 'chriskempson/base16-vim'
Plugin 'ChrisKempson/Tomorrow-Theme'

call vundle#end()
endif
" ------------------------------------------------------------------------ }}}

" -3- General settings ----------------------------------------------------{{{
filetype plugin indent on
if !has("nvim")
set encoding=utf8 " Set utf8 as standard encoding
endi
set history=700 " Sets how many lines of history VIM has to remember

" Use Unix as the standard file type
set ffs=unix,dos,mac

set timeoutlen=500
set ttimeoutlen=0

set cursorline " Highlight current line

set autoread " Set to auto read when a file is changed from the outside

set grepprg=ag " Don't use FINDSTR on windows

" Turn backup off, since most stuff is in SVN, git etc. anyway...
set nobackup
set nowb
set noswapfile

set wildignore+=bundle.js,*.min.js,*.min.css,node_modules/**,bower_components/**,*.swp,*.zip,*.exe

" Specify the behavior when switching between buffers
try
    set switchbuf=useopen,usetab
    set stal=2
catch
endtry

set nosol
set colorcolumn=109
set ttyfast

if !has("nvim")
    set winheight=20
    set winminheight=10
    set winwidth=50
    set winminwidth=10
endif

set listchars=tab:▸\ ,eol:¬,trail:_

" Remember info about open buffers on close
set viminfo^=%

set undofile
let &undodir=VIM_HOME . "/.undo/"

set shortmess=aTtWAIcF
" ------------------------------------------------------------------------ }}}

" -4- Key bindings --------------------------------------------------------{{{
" ---- Visual mode related ---- {{{
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>

" Indent in visual and select mode automatically re-selects
vnoremap > >gv
vnoremap < <gv

" Select (charwise) the contents of the current line, excluding indentation
" http://twitter.com/dotvimrc/status/155748943001694208
nnoremap zv ^vg_

nnoremap zs :SyntasticCheck<CR>

nnoremap z0 :setlocal foldlevel=0<CR>
nnoremap z1 :setlocal foldlevel=1<CR>
nnoremap z2 :setlocal foldlevel=2<CR>
nnoremap z3 :setlocal foldlevel=3<CR>
nnoremap z4 :setlocal foldlevel=4<CR>

"nnoremap <leader>td :noautocmd vimgrep /TODO/j **/*<CR>:cw<CR>

" Reindent/format
vnoremap \f =
nnoremap \f mz=ap`z
nnoremap \F mzgg=G`z

" nmap <leader>/ :set hlsearch<CR>viw*
" TODO things..
" Start the find and replace command across the entire file
vmap <leader>z <Esc>:%s/<c-r>=GetVisual()<cr>/
" ---- }}}

" ---- Moving around, windows, buffers ---- {{{
map <leader>ve :exec "vsplit " . g:my_vimrc<CR>
map <leader>vt :exec "edit " . g:my_vimrc<CR>
map <leader>vE :vsplit $MYVIMRC<CR>
map <leader>vs :source $MYVIMRC<CR>

" Fast saving
nmap <leader>w :w!<cr>

" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

noremap <C-g> g,
noremap <C-f> g;

noremap ; :

noremap \t :enew<CR>
noremap \s :call MkTempFile()<CR>

noremap <leader>bn :bnext<CR>
noremap <leader>bp :bprev<CR>
noremap <leader>bb :ls<CR>:b

" Disable search highlights when <leader><cr> is pressed
noremap <silent> <leader><cr> :noh<cr>

" Remap jumplist so we can take over <Tab>
nnoremap <C-BS> <C-o>
nnoremap <C-CR> <C-i>

" Use <Space> as the easymotion trigger
"map f <Plug>(easymotion-f)
"map <Space> <Plug>(easymotion-s)
"map F <Plug>(easymotion-F)
"map t <Plug>(easymotion-t)
"map T <Plug>(easymotion-T)
" <Leader><Space> triggers easymotion - add an easymotion command afterwards
" eg <Leader><Space>w to trigger word movement
" or <Leader><Space>s for 2-letter search
" map <leader>s <Plug>(easymotion-s)
"nmap <Space> <Plug>(easymotion-s2)
map <Tab> <Plug>(easymotion-s2)
map <C-space> <Plug>(easymotion-s2)

" Remap f/t motions
" nmap t <Plug>(easymotion-bd-tl)
" nmap f <Plug>(easymotion-bd-fl)
" vmap t <Plug>(easymotion-bd-tl)
" vmap f <Plug>(easymotion-bd-fl)
" omap t <Plug>(easymotion-bd-tl)
" omap f <Plug>(easymotion-bd-fl)
" nmap T <Plug>(easymotion-bd-tl)
" nmap F <Plug>(easymotion-bd-fl)
" vmap T <Plug>(easymotion-bd-tl)
" vmap F <Plug>(easymotion-bd-fl)
" omap T <Plug>(easymotion-bd-tl)
" omap F <Plug>(easymotion-bd-fl)

" Word motions
nmap <M-e> <Plug>(easymotion-bd-el)
vmap <M-e> <Plug>(easymotion-bd-el)
omap <M-e> <Plug>(easymotion-bd-el)
nmap <M-w> <Plug>(easymotion-bd-wl)
vmap <M-w> <Plug>(easymotion-bd-wl)
omap <M-w> <Plug>(easymotion-bd-wl)

nmap <M-l> <Plug>(easymotion-lineforward)
nmap <M-j> <Plug>(easymotion-j)
nmap <M-k> <Plug>(easymotion-k)
nmap <M-h> <Plug>(easymotion-linebackward)

omap <M-l> <Plug>(easymotion-lineforward)
omap <M-j> <Plug>(easymotion-j)
omap <M-k> <Plug>(easymotion-k)
omap <M-h> <Plug>(easymotion-linebackward)

vmap <M-l> <Plug>(easymotion-lineforward)
vmap <M-j> <Plug>(easymotion-j)
vmap <M-k> <Plug>(easymotion-k)
vmap <M-h> <Plug>(easymotion-linebackward)

" Smart way to move between windows
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-h> <C-W>h
nnoremap <C-l> <C-W>l

" Switch to MRU buffer
noremap Q :b#<CR>
" noremap W <C-w>p

" Close all the buffers
map <leader>ba :BufOnly<cr>

" Close the current buffer
map <leader>bd :Bclose<cr>

" Use shift-j,k for moving up/down 20 lines
map <s-j> 20j
map <s-k> 20k

" ---- }}}

" ---- Editing keys ---- {{{
" Remap VIM 0 to first non-blank character
noremap 0 ^
noremap ^ 0

" Remap \; to add a semicolon at the EOL in normal mode
nmap \; A;<Esc>

" Use jk/kj to quickly exit insert mode
imap jk <C-c>

nnoremap <expr> i IndentWith("i")
nnoremap <expr> a IndentWith("a")
nnoremap <expr> A IndentWith("A")
" Delete without overwriting the default buffer
nnoremap <leader>x "_x
nnoremap <leader>X "_dd
vnoremap <Leader>x "_x
vnoremap <Leader>X "_dd

nmap \j :%!python -m json.tool<CR>

" Map <C - Enter> in terminal to split at braces
" Note this is remapped in $GVIMRC for windows
inoremap  <CR><Esc>ko


" Join lines
map <leader>lj :call Preserve("join")<CR>
" Split line
map <leader>ls i<CR><Esc>
" Add blank line
nnoremap <leader>. o<Esc>

" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Pressing <leader>t in visual mode opens up a Tabular prompt (try: vip<leader>t)
vmap <Leader>t :Tabular
vmap <Leader>T :Tabular<CR>
" Common use cases..
vmap <Leader>t= :Tabular /=<CR>
vmap <Leader>tf :Tabular /from<CR>

" Quick replay macro in q buffer
nnoremap <leader>q @q

" nnoremap q: <Nop>

" Bubble single lines
nmap <C-up> [e
nmap <C-down> ]e
" Bubble multiple lines
vmap <C-up> [egv
vmap <C-down> ]egv

" Visually select the text that was last edited/pasted
nmap gV `[v`]

nnoremap <Leader>$ :call Preserve("%s/\\s\\+$//e")<CR>

vnoremap $ g_

" Integrate with cygwin/windows http://files.kaoriya.net/goto/vim73w64clipboard
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>y "+Y
nmap <Leader>Y "+Y
nmap <Leader>p "+p
nmap <Leader>P o<Esc>"+p
vmap <Leader>p "+p
vmap <Leader>P "+P
" ---- }}}

" ---- vimgrep searching and cope displaying ---- {{{
" Open vimgrep and put the cursor in the right position
" map <leader>g :Ag // **/*.*<left><left><left><left><left><left><left><left>

" When you press <leader>r you can search and replace the selected text
vnoremap <silent> <leader>r :call VisualSelection('replace')<CR>

map <leader>f :botright cope<cr>
map <leader>cn :cn<cr>
map <leader>cp :cp<cr>

map \dt :diffthis<CR>
map \dd :windo diffthis<CR>
map \do :windo diffoff<CR>
map \dp :diffput<CR>
map \dg :diffget<CR>

" ---- }}}

if has("nvim")
    tnoremap <A-h> <C-\><C-n><C-w>h
    tnoremap <A-j> <C-\><C-n><C-w>j
    tnoremap <A-k> <C-\><C-n><C-w>k
    tnoremap <A-l> <C-\><C-n><C-w>l
    nnoremap <A-h> <C-w>h
    nnoremap <A-j> <C-w>j
    nnoremap <A-k> <C-w>k
    nnoremap <A-l> <C-w>l
endif

" ------------------------------------------------------------------------ }}}

" -5- User interface settings ---------------------------------------------{{{
set so=7 " Set 7 lines to the cursor - when moving vertically using j/k
set wildmenu " Turn on the WiLd menu
set wildignore=*.o,*~,*.pyc " Ignore compiled files
set ruler " Always show current position
set cmdheight=2 " Height of the command bar
set hid " A buffer becomes hidden when it is abandoned

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

set ignorecase " Ignore case when searching
" set smartcase " When searching try to be smart about cases
set hlsearch " Highlight search results
set incsearch " Makes search act like search in modern browsers

" Don't redraw while executing macros (good performance config)
set lazyredraw

set magic " For regular expressions turn magic on
set showmatch " Show matching brackets when text indicator is over them
set mat=2 " How many tenths of a second to blink when matching brackets

" No annoying sound on errors
set noerrorbells
set visualbell
set t_vb=
set tm=500

" Line numbers
set number
set modelines=0

" Folding
set foldmethod=manual
set foldnestmax=8
set foldlevel=5
set foldlevelstart=-1
set foldcolumn=2

" Show non printables
" set list

" Put vsplits on the right
set splitright

" Always show the status line
set laststatus=2

" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l
set statusline+=\ %{exists('g:loaded_fugitive')?fugitive#statusline():''}
" ------------------------------------------------------------------------ }}}

" -6- Colours and fonts -------------------------------------------------- {{{

set t_Co=256
set background=dark
let g:jellybeans_background_color = "252525"
" colorscheme base16-railscasts
colorscheme Tomorrow-Night

" I like my comments green
hi comment term=bold ctermfg=71 guifg=#7f9f4f

" Make cursorline blue
" hi CursorLine term=underline ctermfg=7 ctermbg=235 guibg=#333366
hi ColorColumn term=reverse ctermbg=234 guibg=#344464

" Make folds look nicer
hi Folded term=standout ctermfg=242 ctermbg=234 guifg=#dddddd guibg=#222933

" Make search highlight fit better
hi Search term=reverse ctermfg=235 ctermbg=229 guifg=#afefff guibg=#3f5f9f

" Make popup menu stand out
hi Pmenu ctermfg=12 ctermbg=8 guifg=#d4cfc9 guibg=#303030

" ------------------------------------------------------------------------ }}}

" -7- Tabs and spacing --------------------------------------------------- {{{
set expandtab " Use spaces instead of tabs
set smarttab " Be smart when using tabs ;)

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
"set si "Smart indent
set cindent " C indenting (better for python)
set wrap "Wrap lines

" ------------------------------------------------------------------------ }}}

" -8- Autocomplete settings ---------------------------------------------- {{{
set pumheight=15

" Disable AutoComplPop.
let g:acp_enableAtStartup = 0

" Shell like behavior(not recommended).
set completeopt+=longest
set completeopt-=preview

"set tags+=~/.vim/tags/cpp
"map <C-F12> :!ctags -R --sort=yes --c++-kinds=+pl --fields=+iaS --extra=+q .<CR>

" ------------------------------------------------------------------------ }}}

" -9- Functions ---------------------------------------------------------- {{{
" Smart indent when entering insert mode with i on empty lines
function! IndentWith(command)
    if len(getline('.')) == 0
        if line(".") == line("$")
            return "\"_ddo"
        else
            return "\"_ddO"
        endif
    else
        return a:command
    endif
endfunction

function! MkTempFile()
    let tempFileName = tempname()
    exe ":vsplit " . tempFileName
endfunc

function! Preserve(command)
    " Preparation: save last search, and cursor position.
    let _s=@/
    let l = line(".")
    let c = col(".")
    " Do the business:
    execute a:command
    " Clean up: restore previous search history, and cursor position
    let @/=_s
    call cursor(l, c)
endfunction

" Puts <str> on the command line
function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction

function! ListVEnvs()
    let _envs = split(globpath(expand("$HOME/Envs/"), "*"), "\n")
    for env in _envs
        echo fnamemodify(env, ":t")
    endfor
endfunction

function! LoadVEnv(envname)
    if !has("python") && !has("python3")
        echo "No python interpreter found."
        return
    endif

    let l:envs = split(globpath(expand("$HOME/Envs/"), "*"), "\n")
    let l:found = 0
    let l:fullname = ""
    for env in l:envs
        let l:shortname = fnamemodify(env, ":t")
        if l:shortname == a:envname
            let l:found = 1
            let l:fullname = env
        endif
    endfor

    if !l:found
        echo "Could not find a venv called " . a:envname
        return
    endif

    let g:active_venv = l:fullname
    if has("python3")
        " Add the virtualenv's site-packages to vim path
    py3 << EOF
import os.path
import sys
import vim
project_base_dir = vim.eval("g:active_venv")
print("Activating {0}".format(project_base_dir))
sys.path.insert(0, project_base_dir)
activate_this = os.path.join(project_base_dir, 'Scripts/activate_this.py')
exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'), dict(__file__=activate_this))
EOF
    elseif has("python")
        " Add the virtualenv's site-packages to vim path
    py << EOF
import os.path
import sys
import vim
project_base_dir = vim.eval("g:active_venv")
print("Activating {0}".format(project_base_dir))
sys.path.insert(0, project_base_dir)
activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
execfile(activate_this, dict(__file__=activate_this))
EOF
    endif
endfunction

command! -nargs=1 LoadVEnv call LoadVEnv(<q-args>)
command! ListVEnvs call ListVEnvs()

function! VisualSelection(direction) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    echom l:pattern . "/" . a:direction

    if a:direction == 'b'
        execute "normal ?" . l:pattern . "<CR>"
    elseif a:direction == 'gv'
        call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    elseif a:direction == 'f'
        execute "normal! /" . l:pattern . "<CR>"
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction


" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    en
    return ''
endfunction

" Don't close window, when deleting a buffer
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
    let l:currentBufNum = bufnr("%")
    let l:alternateBufNum = bufnr("#")

    if buflisted(l:alternateBufNum)
        buffer #
    else
        bnext
    endif

    if bufnr("%") == l:currentBufNum
        new
    endif

    if buflisted(l:currentBufNum)
        execute("bdelete! ".l:currentBufNum)
    endif
endfunction

" Escape special characters in a string for exact matching.
" This is useful to copying strings from the file to the search tool
" Based on this - http://peterodding.com/code/vim/profile/autoload/xolox/escape.vim
function! EscapeString (string)
  let string=a:string
  " Escape regex characters
  let string = escape(string, '^$.*\/~[]')
  " Escape the line endings
  let string = substitute(string, '\n', '\\n', 'g')
  return string
endfunction

" Get the current visual block for search and replaces
" This function passed the visual block through a string escape function
" Based on this - http://stackoverflow.com/questions/676600/vim-replace-selected-text/677918#677918
function! GetVisual() range
  " Save the current register and clipboard
  let reg_save = getreg('"')
  let regtype_save = getregtype('"')
  let cb_save = &clipboard
  set clipboard&

  " Put the current visual selection in the " register
  normal! ""gvy
  let selection = getreg('"')

  " Put the saved registers and clipboards back
  call setreg('"', reg_save, regtype_save)
  let &clipboard = cb_save

  "Escape any special characters in the selection
  let escaped_selection = EscapeString(selection)

  return escaped_selection
endfunction

fun! JumpToDef()
  if exists("*GotoDefinition_" . &filetype)
    call GotoDefinition_{&filetype}()
  else
    exe "norm! \<C-]>"
  endif
endf

" ------------------------------------------------------------------------ }}}

" -10- Autocmds ---------------------------------------------------------- {{{
" Return to last edit position when opening files (You want this!)
augroup vimrcgroup
    autocmd!

    autocmd BufReadPost *
                \ if line("'\"") > 0 && line("'\"") <= line("$") |
                \   exe "normal! g`\"" |
                \ endif

    " Trim spaces
    autocmd BufWrite *.py :call Preserve("%s/\\s\\+$//e")
    autocmd BufWrite *.coffee :call Preserve("%s/\\s\\+$//e")
    autocmd BufWrite *.js :call Preserve("%s/\\s\\+$//e")

    " if has("autocmd") && exists("+omnifunc")
    " autocmd Filetype *
                    " \    if &omnifunc == "" |
                    " \    setlocal omnifunc=syntaxcomplete#Complete |
                    " \    endif
    " endif

    if has("autocmd")
    autocmd FileType *
                \ if &l:buftype == "" && &l:filetype != "qf" && &l:filetype != "startify" |
                \   nnoremap <buffer> <CR> ;|
                \   nnoremap <buffer> <BS> ,|
                \   vnoremap <buffer> <CR> ;|
                \   vnoremap <buffer> <BS> ,|
                \ endif
    endif

    autocmd BufWinEnter netrw nnoremap <buffer> q :q<CR>

    autocmd BufWinEnter quickfix nnoremap <buffer> q :cclose<CR>

    au BufNewFile,BufRead *.xaml setf xml

    autocmd BufReadPost *.tag set ft=html

    " Folding
    autocmd FileType javascript setlocal foldmethod=syntax
    autocmd FileType css setlocal foldmethod=syntax
    autocmd FileType python setlocal foldmethod=indent

    autocmd FileType html,xhtml,xml,htmldjango setlocal foldmethod=indent
    autocmd FileType html,xhtml,xml,htmldjango setlocal foldnestmax=20

    " Enable folding by markers for vim files
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType vim setlocal foldlevel=0

    " autocmd FileType python setlocal completeopt-=preview

    autocmd BufReadPost *.jade set ft=jade
    autocmd FileType jade setlocal tabstop=2
    autocmd FileType jade setlocal shiftwidth=2

    " autocmd BufWritePost *.coffee CoffeeLint

    "autocmd BufEnter * silent! lcd %:p:h

    " Jump to tag
    autocmd FileType c nnoremap <buffer> <Leader>t :call JumpToDef()<cr>
    autocmd FileType cpp nnoremap <buffer> <Leader>t :call JumpToDef()<cr>

    if has('autocmd')
      function! ILikeHelpToTheRight()
        if !exists('w:help_is_moved') || w:help_is_moved != "right"
          wincmd L
          let w:help_is_moved = "right"
        endif
      endfunction

        autocmd FileType help nested call ILikeHelpToTheRight()
    endif

    autocmd WinEnter * call s:CloseIfOnlyNerdTreeLeft()

    " Close all open buffers on entering a window if the only
    " buffer that's left is the NERDTree buffer
    function! s:CloseIfOnlyNerdTreeLeft()
        if exists("t:NERDTreeBufName")
            if bufwinnr(t:NERDTreeBufName) != -1
                if winnr("$") == 1
                    q
                endif
            endif
        endif
    endfunction

    autocmd WinEnter * set cul
    autocmd WinLeave * set nocul
augroup END
" ----------------------------------------------------------------------- }}}

