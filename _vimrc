" Some fixes for Windows gVim.
if has("gui_running")
    "g:my_vim_dir is used elsewhere in my vim configurations
    let g:my_vim_dir=expand("$HOME/.vim")

    "$HOME/.vim and $HOME/.vim/after are in the &rtp on unix
    "But on windows, they need to be added.
    if has("win16") || has("win32") || has("win64")
      "add g:my_vim_dir to the front of the runtimepath
       execute "set rtp^=".g:my_vim_dir
      "add g:my_vim_dir\after to the end of the runtimepath
      execute "set rtp+=".g:my_vim_dir."\\after"
      "Note, pathogen#infect() looks for the 'bundle' folder in each path
      "of the &rtp, where the last dir in the '&rtp path' is not 'after'. The
      "<path>\bundle\*\after folders will be added if and only if
      "the corresponding <path>\after folder is in the &rtp before
      "pathogen#infect() is called.  So it is very important to add the above
      "'after' folder.
      "(This applies to vim plugins such as snipmate, tabularize, etc.. that
      " are loaded by pathogen (and perhaps vundle too.))

      " Not necessary, but I like to cleanup &rtp to use \ instead of /
      " when on windows machines
      let &rtp=substitute(&rtp,"[/]","\\","g")

      "On windows, if called from cygwin or msys, the shell needs to be changed
      "to cmd.exe to work with certain plugins that expect cmd.exe on windows versions
      "of vim.
      if &shell=~#'bash$'
    set shell=$COMSPEC " sets shell to correct path for cmd.exe
      endif
    endif

    " set guifont=DejaVu_Sans_Mono_for_Powerline:h10:cANSI
    set guifont=Fantasque_Sans_Mono:h13:cANSI
    set guioptions-=T
    set guioptions=
    " set guioptions+=m " Add menubar
    " set guioptions+=e " Add gui tabs
    set guitablabel=%M\ %t
    set noerrorbells visualbell t_vb=
    set mouse=""
    autocmd GUIEnter * set visualbell t_vb=
    inoremap <C-> <CR><Esc>ko
    set noshellslash
    set columns=140
    set lines=60
endif

exec "source " . expand("$HOME/.vim")."/vimrc"
let g:my_vimrc=expand("$HOME/.vim")."/vimrc"
